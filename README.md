# fastapi-amis-webframe

## 介绍

一个基于fastapi和amis框架的后台脚手架，方便大家使用。

目前是实现了基础的登陆登出。

基于本脚手架可以方便快捷的开始你自己的后台管理服务的开发工作。

## 反向代理
```
例如： http://amis.51znet.com ==> http://127.0.0.1:8855
location /
{
    proxy_pass http://127.0.0.1:8855;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header REMOTE-HOST $remote_addr;
    
    add_header X-Cache $upstream_cache_status;
    
    #Set Nginx Cache
    
    add_header Cache-Control no-cache;
}

```

## 运行登录
```
- cd server/app
- python main.py &
- open demo, http://amis.51znet.com
- login with linlw@qq.com / 123456
- open docs, http://amis.51znet.com/docs
```

## 修改配置
- 网址： server/app/templates/sdk/vars.js
    var baseUrl = 'http://amis.51znet.com'
- 数据： server/app/database.py
    values = {
        "email": "linlw@qq.com",
        "hashed_password": get_password_hash('123456'),
        "is_active": True
    }
- 环境： server/app/main.py
    uvicorn.run("main:app", host="127.0.0.1", port=8855, reload=True, log_level="info")
- 权限： server/app/templates/admin.html
    "visibleOn": "email == \"linlw@qq.com\""  
## 使用框架

- fastapi
- amis

## 已完成功能

- 自动初始化sqlite数据库
- 接口验证
- 自动跳转登录页
- 登陆后自动跳转主界面，并显示登陆用户邮箱信息
- 登出后自动跳转回登录页
- 系统用户管理功能

## 待开发功能

- 系统用户权限功能
- 用户自我信息维护功能
- ...

## 2021-06-24 update
```
1.upgraded to amis sdk v1.1.7
2.copy old templates/sdk/vars.js to sdk/
```

## 2021-06-25 update
```
$ pip install pipreqs
$ pipreqs /path/to/project
==> requirements.txt
```
